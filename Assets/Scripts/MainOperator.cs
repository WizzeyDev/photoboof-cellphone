﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using System.IO;

public class MainOperator : MonoBehaviour
{
    #region Public Fields
    public GameObject uiCanvas;
    UnityEngine.Video.VideoPlayer videoPlayer,videoPlayer2;
    public VideoClip[] videoClips;
    public TMPro.TextMeshProUGUI countDownTxt;
    public AudioClip cameraClick;
    public VoxelBusters.Demos.ReplayKit.ReplayKitDemo replayKitDemo;
    #endregion

    #region Private Fields
    Vector2 firstTouchPositsion;
    bool isTouching,isScrollingVideos = true,isSwapingVideo,countDown;
    float touchedThreshold = 25, touchDistance,videoOutOfBoundY = 12;
    float pointOutOfBountds, Speed;
   public int currentVideoPLayed,screenAmountShot;
    string cameraFolderPath /*test2Folder*//*,regulareGalleryPath = "storage/emulated/0/DCIM/Camera", ExternallGalleryPath*/;
    AudioSource audioSource;
    #endregion

    #region Mono
    //private void Awake() {
    //    RefindReplayKit();
    //}

    void Start()
    {
        if (replayKitDemo != null)
        {

        replayKitDemo.Initialise(); //Start the Video Captureing
        }

        Invoke("RefindReplayKit", 5f);
        //getSocialCapture = GetComponent<GetSocialSdk.Capture.Scripts.GetSocialCapture>();
        //getSocialCapturePreview = GetComponent<GetSocialSdk.Capture.Scripts.GetSocialCapturePreview>();
        if (audioSource == null)
        {
            print("AUDIo");
            audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            audioSource.loop = false;
            audioSource.volume = 50f;
            audioSource.clip = cameraClick;
        }

        if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(UnityEngine.Android.Permission.ExternalStorageWrite))
        {
            UnityEngine.Android.Permission.RequestUserPermission(UnityEngine.Android.Permission.ExternalStorageWrite);
            UnityEngine.Android.Permission.RequestUserPermission(UnityEngine.Android.Permission.ExternalStorageRead);
        }
        //videoPlayer = GameObject.Find(UnityEngine.Video.VideoPlayer)
        videoPlayer = Object.FindObjectOfType<VideoPlayer>();
        if (videoPlayer != null)
        {
            if (isScrollingVideos)
            {
                videoPlayer.isLooping = true;
                InitVideo2();
            }
            else
            {
            videoPlayer.loopPointReached += EndReached;
            videoPlayer.isLooping = false;

            }
            InitVideoPlayer1Clip();
        
            //videoPlayer2.clip = videoClips[1];

            //videoPlayer2.Play();
        }

        videoPlayer.transform.position = new Vector3(25, 0, 0);
        Invoke("BackFromInvisable", 1f);
        cameraFolderPath = Application.persistentDataPath;
        //cameraFolderPath = DiskOnKey.GetAndroidExternalFilesDir();
        Debug.Log("DiskOnKey: " + cameraFolderPath);
        
        string sdName = Directory.GetParent(cameraFolderPath).Parent.Parent.Parent.Name;

        int placeOfName = cameraFolderPath.IndexOf(sdName);
        cameraFolderPath = cameraFolderPath.Substring(0, placeOfName) + sdName;
        Debug.Log("<color=red>3 </color> " + cameraFolderPath);
        //test2Folder = cameraFolderPath;
    
        var main = Directory.GetDirectories(cameraFolderPath, "Camera", SearchOption.AllDirectories);
        foreach (var item in main)
        {
            cameraFolderPath = item;
            Debug.Log("<color=blue>Main </color> " + cameraFolderPath);
        }
 
 
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {   
               var touch = Input.touches[0];
            if (touch.phase == TouchPhase.Began)
            {
                Debug.Log("Touching started");
                firstTouchPositsion = touch.deltaPosition;
                touchDistance = 0;
            }
            if (touch.phase == TouchPhase.Moved)
            {
                touchDistance += touch.deltaPosition.y;
                if (touchDistance > touchedThreshold) //Scroll Down
                {
                    Debug.Log("Scroll Down ");
                    VideoSwaping(false);
                }
                if (touchDistance < (-touchedThreshold)) //Scroll Up
                {
                    Debug.Log("Scroll Up ");
                    VideoSwaping(true);
                }
                //Debug.Log("Touching Diffrance " + firstTouchPositsion + " Now: " + touch.deltaPosition.y + " Dif " + (firstTouchPositsion.y - touch.deltaPosition.y));
            }
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            replayKitDemo = Object.FindObjectOfType<VoxelBusters.Demos.ReplayKit.ReplayKitDemo>();
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            Debug.Log("Scroll Up ");
            VideoSwaping(true);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("Scroll Down ");
            VideoSwaping(false);
        }
    }

    #endregion Mono


    #region Init

    //GetSocialSdk.Capture.Scripts.GetSocialCapture getSocialCapture;
    //GetSocialSdk.Capture.Scripts.GetSocialCapturePreview getSocialCapturePreview;
    // Start is called before the first frame update
    void RefindReplayKit() {
        Debug.Log("<color=red>4 </color> ");
        if (replayKitDemo == null) {
            replayKitDemo = Object.FindObjectOfType<VoxelBusters.Demos.ReplayKit.ReplayKitDemo>();
        }
      
    }

    void InitVideoPlayer1Clip() {
        videoPlayer.clip = videoClips[videoClips.Length-1];
        videoPlayer.Play();
    }

    void InitVideo2() {
        videoPlayer2 = Instantiate(videoPlayer.gameObject).GetComponent<VideoPlayer>();
        RenderTexture rt = new RenderTexture(videoPlayer.targetTexture);
        rt.Create();
        Material newMat = new Material(videoPlayer.GetComponent<MeshRenderer>().material);
        newMat.mainTexture = rt;
        videoPlayer2.GetComponent<MeshRenderer>().material = newMat;
        videoPlayer2.targetTexture = rt;
    }

   void VideoPLayerTextureWorkAround() {
        currentVideoPLayed = videoClips.Length-1;

            videoPlayer.transform.position = new Vector3(-(1.4f), videoPlayer.transform.position.y, videoPlayer.transform.position.z);
        videoPlayer2.transform.position = new Vector3(-(1.4f), videoPlayer2.transform.position.y, videoPlayer2.transform.position.z);
    }


    void BackFromInvisable() {
        Debug.Log("D?");
        VideoSwaping(true);
        Invoke("VideoPLayerTextureWorkAround",2.5f);
    }


    #endregion Init
    void VideoSwaping(bool scrolledUp)
    {
      
        if (!isSwapingVideo)
        {
            isSwapingVideo = true;
            if (videoPlayer.isPlaying)
        {
            videoPlayer2.gameObject.SetActive(true);
            videoPlayer.Pause();
            LoadNextVideo(videoPlayer2, scrolledUp);
            if (scrolledUp)
            {
                    videoPlayer2.transform.position = new Vector3(videoPlayer.transform.position.x, videoOutOfBoundY, videoPlayer.transform.position.z);
                    //PlayScrollAnimastion(scrolledUp, videoPlayer.GetComponent<Animator>(), videoPlayer2.GetComponent<Animator>());
                    ScrollAnim(scrolledUp, videoPlayer.transform, videoPlayer2.transform);
                }
            else
            {
                    videoPlayer2.transform.position = new Vector3(videoPlayer.transform.position.x, -videoOutOfBoundY, videoPlayer.transform.position.z);
                    //PlayScrollAnimastion(scrolledUp,  videoPlayer.GetComponent<Animator>(), videoPlayer2.GetComponent<Animator>());
                    ScrollAnim(scrolledUp, videoPlayer.transform, videoPlayer2.transform);
                }
            //videoPlayer2.clip()
        }else if (videoPlayer2.isPlaying)
        {
            videoPlayer.gameObject.SetActive(true);
            videoPlayer2.Pause();
            LoadNextVideo(videoPlayer, scrolledUp);
            if (scrolledUp)
            {
                    videoPlayer.transform.position = new Vector3(videoPlayer.transform.position.x, videoOutOfBoundY, videoPlayer.transform.position.z);
                    //PlayScrollAnimastion(scrolledUp,  videoPlayer2.GetComponent<Animator>(),videoPlayer.GetComponent<Animator>());
                    ScrollAnim(scrolledUp, videoPlayer2.transform, videoPlayer.transform);

                }
            else
            {
                    videoPlayer.transform.position = new Vector3(videoPlayer.transform.position.x, -videoOutOfBoundY,videoPlayer.transform.position.z);
                    Debug.Log("??");
                    //PlayScrollAnimastion(scrolledUp, videoPlayer2.GetComponent<Animator>(), videoPlayer.GetComponent<Animator>());
                    ScrollAnim(scrolledUp, videoPlayer2.transform, videoPlayer.transform);
                }

            }
        else
        {
            Debug.Log("No Video Playing?");
        }

        }
        //Invoke("TurnOffAnim", 1.5f);
    }

    void TurnOffAnim()
    {
        Debug.Log("isSwapingVideo");
        isSwapingVideo = false;
    }


    void PlayScrollAnimastion(bool scrolledUp,Animator animOut,Animator AnimIn)
    {
        if (scrolledUp)
        {
            animOut.SetTrigger("SlideOutDown");
            AnimIn.SetTrigger("SlideInUp");
        }
        else
        {
            animOut.SetTrigger("SlideOutUp");
            AnimIn.SetTrigger("SlideInDown");
        }
    }

    void ScrollAnim(bool scrolledUp,Transform tranOut, Transform tranIn)
    {
        if (scrolledUp)
        {
            StartCoroutine("SlideInSFX", new SliderObject(tranOut, -(videoOutOfBoundY), scrolledUp));
            //tranOut.SetTrigger("SlideOutDown");
            //tranIn.SetTrigger("SlideInUp");
            StartCoroutine("SlideInSFX", new SliderObject(tranIn, videoQuadDefualtY, scrolledUp));
        }
        else 
        {
            StartCoroutine("SlideInSFX", new SliderObject(tranOut, videoOutOfBoundY, scrolledUp));
            //tranOut.SetTrigger("SlideOutUp");
            //tranIn.SetTrigger("SlideInDown");
            StartCoroutine("SlideInSFX", new SliderObject(tranIn, videoQuadDefualtY, scrolledUp));
        }
    }

    public float speed = 20,videoQuadDefualtY = -1.4f;

    IEnumerator SlideInSFX(SliderObject ObjectToMove)
    {
        Vector3 targetLOc = new Vector3(ObjectToMove.objectToMove.position.x, ObjectToMove.targetLoc, ObjectToMove.objectToMove.position.z);
        print("Started SlideInSFX" +  ObjectToMove.objectToMove.name + ObjectToMove.objectToMove.transform.position.y + " target " + ObjectToMove.targetLoc);
        //if (ObjectToMove.objectToMove.transform.position.y == videoQuadDefualtY && ObjectToMove.targetLoc == videoQuadDefualtY)
        //{
        //    print("Started Changed Loc?");
        //    if (ObjectToMove.isUserScrolledUP)
        //    {
        //        ObjectToMove.objectToMove.transform.position = new Vector3 (ObjectToMove.objectToMove.position.x,videoOutOfBoundY, ObjectToMove.objectToMove.position.z);
        //    }
        //    else
        //    {
        //        ObjectToMove.objectToMove.transform.position = new Vector3(ObjectToMove.objectToMove.position.x, -(videoOutOfBoundY), ObjectToMove.objectToMove.position.z);
        //    }
        //}
        //else
        //{
        //    if (true)
        //    {

        //    }
        //    print("SlideInSFX??:");
        //}
        while (true)
        {
        ObjectToMove.objectToMove.transform.position = Vector3.MoveTowards(ObjectToMove.objectToMove.transform.position, targetLOc,speed*Time.deltaTime);
            //print("Moved: " +  ObjectToMove.objectToMove.transform.position + " Name: " + ObjectToMove.objectToMove.gameObject.name);
            yield return null;
            if (Vector3.Distance(ObjectToMove.objectToMove.position, targetLOc) <= 0.01f && Vector3.Distance(ObjectToMove.objectToMove.position, targetLOc) >= -(0.01f))
            {
                print("Done :");
                break;
            }
        }
        ObjectToMove.objectToMove.transform.position = targetLOc;
        print("Done SlideInSFX:" + ObjectToMove.objectToMove.name);
        isSwapingVideo = false;
        yield return null;
    }

    public void SaveFile()
    {
        Debug.Log("Stoping");
        //getSocialCapture.StopCapture();

        System.Action<byte[]> result = bytes =>{};

        //getSocialCapture.GenerateCapture(result);
    }

 

    void PlayVideo()
    {
        videoPlayer.Play();
    }


    public void TakeAScreenShot()
    {
        audioSource.Play();
        for (int i = 0; i < uiCanvas.transform.childCount; i++)
        {
            uiCanvas.transform.GetChild(i).gameObject.SetActive(false);
        }

        Debug.Log("takeing Pic");
        StartCoroutine("TakeScreenShot");
       //Invoke("DelayedScreenShot",0.04f);
       
    }

    public void TakeVideo()
    {
        GetComponent<GetSocialSdk.Capture.Scripts.GetSocialCapture>().StartCapture();
       Debug.Log("START VIDEO");
    }

    public void InitCaptureing()
    {
        if (!countDown)
        {
            countDown = true;
            if (true) {

            }
        StartCoroutine("CountDown", 4);
        }
        //replayKitDemo.StartRecording();
    }

    int IsVideoPLaying() {
        if (videoPlayer.isPlaying) {
            return (int)videoPlayer.clip.length;
        }
        else if (videoPlayer2.isPlaying) {
            return (int)videoPlayer2.clip.length;
        }
        else {
            return 10;
        }
    }

    void StartCaptureing()
    {
        uiCanvas.SetActive(false);
        if (videoPlayer.isPlaying) {
            videoPlayer.time = 0f;
        }
        else if (videoPlayer2.isPlaying) {
            videoPlayer2.time = 0f;
        }
        replayKitDemo.StartRecording();
    }

    void StopCaptureing()
    {
        replayKitDemo.StopRecording();
        uiCanvas.SetActive(true);
        countDown = false;
        //AudioListener.volume = 1f; videoPlayer.SetDirectAudioVolume(0, 1f);
        //videoPlayer2.SetDirectAudioVolume(0, 1f);
    }

   

    IEnumerator CountDown(int countDown)
    {

       
        Debug.Log("Start of countdown" + countDown);
      
        countDownTxt.gameObject.SetActive(true);
        float x = countDown;
        while (x >= 0f)
        {
            countDownTxt.text = ((int)x).ToString();
            x -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForEndOfFrame();
        Debug.Log("End of countdown");
        //videoPlayer.SetDirectAudioVolume(0,0.2f);
        //videoPlayer2.SetDirectAudioVolume(0, 0.2f);
        //AudioListener.volume = 0.2f;
        countDownTxt.gameObject.SetActive(false);
        StartCaptureing();
        Invoke("StopCaptureing", IsVideoPLaying());
        yield return null;
    }

    IEnumerator TakeScreenShot()
    {
        yield return new WaitForEndOfFrame();
        string path = "IMG_SB";
        //path = cameraFolderPath + path + System.DateTime.Now.ToString("yytyyMMmm_hhmmss") + ".jpg";
        //Debug.Log("TookPic" + path);
        ScreenCapture.CaptureScreenshot(path);
     
        //Debug.Log("CreateDirectory" + cameraFolderPath + "/StarsBox");
        string screenShot = Application.persistentDataPath + "/" + path;

        if (File.Exists(screenShot))
        {
            Debug.Log("Found New Pic Application.persistentDataPath" + screenShot);
            //File.Copy(screenShot, "storage/emulated/0/DCIM/Camera" + "/" + path + ".jpg", true);
            //File.Copy(screenShot, test2Folder + "/" + path + ".jpg", true);
            string newSSName = cameraFolderPath + "/" + path + System.DateTime.Now.ToString("yytyyMMmm_hhmmss") + ".jpg";

            //if (Directory.Exists(regulareGalleryPath))
            //{
            //    Debug.Log("Saveing pic " + regulareGalleryPath + newSSName);
            //    File.Copy(screenShot, regulareGalleryPath + newSSName, true); //"storage/emulated/0/DCIM/Camera/"

            //}

            if (Directory.Exists(cameraFolderPath))
            {
                try
                {

                    File.Copy(screenShot, newSSName, true);
                    Debug.Log("Externall Copy" + newSSName);
                }
                catch (System.Exception)
                {
                    Debug.LogError("Didnt Copy " + newSSName);
                    throw;
                }
            }
        }
        else
        {
            screenShot = Application.dataPath + "/" + path;

            if (File.Exists(screenShot))
            {

                Debug.Log("Found New Pic Application.dataPath" + screenShot);
                string newSSName = cameraFolderPath + "/" + path + System.DateTime.Now.ToString("yytyyMMmm_hhmmss") + ".jpg";

                //if (Directory.Exists(regulareGalleryPath))
                //{
                //    Debug.Log("Saveing pic " + regulareGalleryPath + newSSName);
                //    File.Copy(screenShot, regulareGalleryPath + newSSName, true); //"storage/emulated/0/DCIM/Camera/"

                //}

                if (Directory.Exists(cameraFolderPath))
                {
                    try
                    {

                        File.Copy(screenShot, newSSName, true);
                        Debug.Log("dataPath Copy" + newSSName);
                    }
                    catch (System.Exception)
                    {
                        Debug.LogError("Didnt Copy");
                        throw;
                    }
                }

                //    Debug.Log("Made New Pic In gallery??");
                //    string newSSName = "/" + path + System.DateTime.Now.ToString("yytyyMMmm_hhmmss") + ".jpg";
                //    //if (Directory.Exists(regulareGalleryPath))
                //    //{

                //    //    File.Copy(screenShot, regulareGalleryPath + newSSName, true); //"storage/emulated/0/DCIM/Camera/"

                //    //}

                //    if (Directory.Exists(cameraFolderPath))
                //    {
                //        try
                //        {

                //            File.Copy(screenShot, cameraFolderPath + newSSName, true);
                //            Debug.Log("Externall Copy" + cameraFolderPath + newSSName);
                //        }
                //        catch (System.Exception)
                //        {

                //            throw;
                //        }
                //    }
            }
            else
            {
                Debug.Log("Didnt Find in Application.dataPath");
            }
            Debug.Log("Didnt Find CamFolder" + cameraFolderPath);

        }

        for (int i = 0; i < uiCanvas.transform.childCount; i++)
        {
            uiCanvas.transform.GetChild(i).gameObject.SetActive(true);
        }
        yield return new WaitForEndOfFrame();
        DelayFlash();
        yield return new WaitForEndOfFrame();
        //Invoke("DelayFlash", 0.05f);

        //File.Delete(screenShot);
        yield return null;
    }

   

    void DelayFlash()
    {
        PhoneCamera.RunFlash();
    }

    void EndReached(VideoPlayer vp) {
        currentVideoPLayed++;
        if (currentVideoPLayed >= videoClips.Length)
        {
            currentVideoPLayed = 0;
        }
        vp.clip = videoClips[currentVideoPLayed];
        vp.Play();
    }

    void LoadNextVideo(VideoPlayer vp,bool isUp) {
        if (isUp)
        {
            currentVideoPLayed++;
            if (currentVideoPLayed >= videoClips.Length)
            {
                currentVideoPLayed = 0;
            }
        }
        else
        {
            currentVideoPLayed--;
            if (currentVideoPLayed < 0)
            {
                currentVideoPLayed = videoClips.Length-1;
            }
        }
        Debug.Log("CVideo " + currentVideoPLayed);
        vp.clip = videoClips[currentVideoPLayed];
        vp.Play();
    }

    public void ExitApp()
    {
        Application.Quit();
    }
}


public class SliderObject
{
    public Transform objectToMove;
    public float targetLoc;
    public bool isUserScrolledUP,isSlideIn;

    public SliderObject(Transform tran , float loc,bool isScrolledUp) {
        objectToMove = tran;
        targetLoc = loc;
        isUserScrolledUP = isScrolledUp;
    }
}