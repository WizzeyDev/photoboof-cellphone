﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class DiskOnKey
{
    public static string cliendInfosPath;

    public static void CheckIniFile()
    {
        //if (!File.Exists(Application.persistentDataPath + "/cliendInfosPath.txt")) {
        //    File.Create(Application.persistentDataPath + "/cliendInfosPath.txt");
        //}
        //cliendInfosPath = Application.persistentDataPath + "/cliendInfosPath.txt";
#if UNITY_EDITOR
#endif
        if (!File.Exists(Application.streamingAssetsPath + "/cliendInfosPath.txt"))
        {
            File.Create(Application.streamingAssetsPath + "/cliendInfosPath.txt");
        }
        cliendInfosPath = Application.streamingAssetsPath + "/cliendInfosPath.txt";
    }


    public static void SaveClient(string name, string email, string path)
    {
        StreamWriter writer = new StreamWriter(cliendInfosPath, true);

        string me = string.Format("[{0};{1};{2}]", name, email, path);
        writer.WriteLine(me);
        writer.Close();
    }

    public static string ReadString(string pathToSplit)
    {

        Debug.Log("Starting Reading " + pathToSplit);

        //pathToSplit.Trim('\n');
        //    Debug.Log("Check "+ pathToSplit);
        //if (pathToSplit.StartsWith("\n")) {
        //}
        //else {
        //    pathToSplit.TrimStart('\n');
        //}
        // char[] be = { '[', ']' };
        var clientInfos = pathToSplit.Split('/');
        foreach (var item in clientInfos)
        {
            Debug.Log("<color=blue>Splited: </color>" + item);
            if (!string.IsNullOrEmpty(item))
            {
                //if (!item.Contains("[" + "]") && item.Contains(";")) {

                ////Debug.Log(item);
                //}
            }
        }


        return pathToSplit;
    }

    public static string GetAndroidExternalFilesDir()
    {
        using (AndroidJavaClass unityPlayer =
               new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject context =
                   unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                // Get all available external file directories (emulated and sdCards)
                AndroidJavaObject[] externalFilesDirectories =
                                    context.Call<AndroidJavaObject[]>
                                    ("getExternalFilesDirs", (object)null);
                AndroidJavaObject emulated = null;
                AndroidJavaObject sdCard = null;
                for (int i = 0; i < externalFilesDirectories.Length; i++)
                {
                    AndroidJavaObject directory = externalFilesDirectories[i];
                    using (AndroidJavaClass environment =
                           new AndroidJavaClass("android.os.Environment"))
                    {
                        // Check which one is the emulated and which the sdCard.
                        bool isRemovable = environment.CallStatic<bool>
                                          ("isExternalStorageRemovable", directory);
                        bool isEmulated = environment.CallStatic<bool>
                                          ("isExternalStorageEmulated", directory);
                        if (isEmulated)
                            emulated = directory;
                        else if (isRemovable && isEmulated == false)
                            sdCard = directory;
                    }
                }
             


                // Return the sdCard if available
                if (sdCard != null) {
                    
                    return sdCard.Call<string>("getAbsolutePath");
                }
                else
                {
                   
                return emulated.Call<string>("getAbsolutePath");

                }
            }
        }

    }
}
