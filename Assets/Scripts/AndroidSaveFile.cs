﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace utilities
{
   

    public static class AndroidSaveFile
    {
        #region ----Public Fields----
        public static string mainFolder;
        public static string externalDrive, regulareGalleryPath;

        #endregion ----Public Fields----


        public static void InitAppPath()
        {
            externalDrive = DiskOnKey.GetAndroidExternalFilesDir();
            Debug.Log("<color=blue>InitAppPath </color>" + externalDrive);
            //externalDrive = Directory.GetParent(externalDrive).Parent.Parent.Parent.Name;
            string sdName = Directory.GetParent(mainFolder).Parent.Parent.Parent.Name;
            int placeOfName = mainFolder.IndexOf(sdName);
            mainFolder = mainFolder.Substring(0, placeOfName);
            mainFolder += sdName;    
            Debug.Log("<color=blue>Main </color> " + mainFolder);
            var main = Directory.GetDirectories(mainFolder, "Camera", SearchOption.AllDirectories);
            foreach (var item in main)
            {
                Debug.Log("Find Folder Internal: " + item);
                regulareGalleryPath = item;
            }
            sdName = Directory.GetParent(externalDrive).Parent.Parent.Parent.Name;
            placeOfName = externalDrive.IndexOf(sdName);
            externalDrive = mainFolder.Substring(0, placeOfName);
            externalDrive += sdName;
            Debug.Log("<color=red>Find Folder External?: </color>" + externalDrive);
            var directoryInfo = Directory.GetDirectories(externalDrive, "Camera", SearchOption.AllDirectories);
            foreach (var item in directoryInfo)
            {
                externalDrive = item;
                Debug.Log("Find Folder External: " + item);
            }
        }

        public static void SaveFile(string filePath)
        {
            if (File.Exists(filePath))
            {
                Debug.Log("Made New Pic In gallery??");

                if (Directory.Exists(filePath))
                {

                    File.Copy(filePath, regulareGalleryPath + Path.GetFileName(filePath), true); //"storage/emulated/0/DCIM/Camera/"

                }

                if (Directory.Exists(externalDrive))
                {
                    try
                    {

                        File.Copy(filePath, externalDrive + Path.GetFileName(filePath), true);
                        Debug.Log("Externall Copy" + externalDrive + filePath);
                    }
                    catch (System.Exception)
                    {

                        throw;
                    }
                }
            }

        }
    }
}