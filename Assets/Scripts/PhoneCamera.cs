﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneCamera : MonoBehaviour
{
    #region Serializable Fields
    public RawImage displayImage;
    public bool gotPermission;
    public float flashSpeed;
    public TMPro.TextMeshProUGUI debugText;
    #endregion

    #region Private Fields
    WebCamDevice[] devices;
    private WebCamTexture webCameraTexture;
    RectTransform webCamRect;
    public RawImage flashImage;
    char AltDirectorySeparatorChar;
    private static string deviceName = string.Empty;
    int currentWebCam;
    bool isFlashing,isPlayingCamera;
    float flashyStart ,minHeight = 11.8f, minWeight = 6.5f;
    static PhoneCamera instace;
    #endregion

    private void Awake()
    {
#if UNITY_ANDOIRD || UNITY_EDITOR

        AltDirectorySeparatorChar = '/';
#endif

#if UNITY_IOS

        AltDirectorySeparatorChar = '\\';
#endif
    }

    // Start is called before the first frame update
    void Start()
    {
        instace = this;

        if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(UnityEngine.Android.Permission.Camera))
        {
            UnityEngine.Android.Permission.RequestUserPermission(UnityEngine.Android.Permission.Camera);
            Application.LoadLevel(0);
        }
        else
        {
            gotPermission = true;
            InitCamera();
        }
    }

    // Update is called once per frame
    void Update()
    {
#if PLATFORM_ANDROID
   
       

        //if (Input.touchCount > 0)
        //{
        //    //webCameraTexture.Stop();
        //    if (webCameraTexture.isPlaying)
        //    {
        //        webCameraTexture.Stop();
        //    }
        //    else
        //    {
        //        webCameraTexture.Play();
        //    }
        //}

#endif

        //if (Input.touchCount > 0)
        //{
        //    print("Checking WebCam" + " Mirroed " + webCameraTexture.videoVerticallyMirrored + " RotationAngle " + webCameraTexture.videoRotationAngle  + " width: " + (float)webCameraTexture.width + " Height: " + (float)webCameraTexture.height);
        //}

        if (isFlashing)
        {

            flashImage.material.color = Color32.Lerp(Color.white, Color.clear, (Time.timeSinceLevelLoad - flashyStart) * flashSpeed);
            //flashImage.material.color= Color.clear

            if (flashImage.color == Color.clear)
            {
                print("Flash Done");
                isFlashing = false;
            }
        }
    }

    public void PlayWeb()
    {
        webCameraTexture.Play();
    }

    public void StopWeb()
    {
        webCameraTexture.Stop();
    }
    public void ChangeCamera()
    {
        print("Change Camera");
        webCameraTexture.Stop();
        currentWebCam++;
        if (currentWebCam >= devices.Length)
        {
            currentWebCam = 0;
        }
        webCameraTexture.deviceName = devices[currentWebCam].name;
        //Vector2 size = GetComponent<RectTransform>().sizeDelta = new Vector2(1920, 1080);
        //displayImage.material.mainTexture = webCameraTexture;
        webCameraTexture.Play();
        
        if (!webCameraTexture.videoVerticallyMirrored)
        {
            displayImage.uvRect = new Rect(1, 0, -1, -1);

        }
        else
        {
            displayImage.uvRect = new Rect(0, 0, 1, -1);
        }
        print("<color=blue>WebCamTexture.devices.Length </color>" + WebCamTexture.devices.Length);
        print("Checking WebCam" + " Mirroed " + webCameraTexture.videoVerticallyMirrored + " RotationAngle " + webCameraTexture.videoRotationAngle + " Height: "  + (float)webCameraTexture.height);
        webCamRect.localEulerAngles = new Vector3(0f, 0f, webCameraTexture.videoRotationAngle);
        //StartCoroutine("WebCamSetup");
    }

    void InitCamera() {
        //WebCamDevice[] devices = WebCamTexture.devices;
        devices = WebCamTexture.devices;
        //debugText.text = devices.Length.ToString();
        print("WebCamTexture.devices.Length " + WebCamTexture.devices.Length);
        webCamRect = displayImage.GetComponent<RectTransform>();

        if (devices.Length > 0)
        {
            webCameraTexture = new WebCamTexture();
            for (int i = 0; i < devices.Length; i++)
            {
                foreach (var item in devices[i].availableResolutions)
                {
                    print("<color=blue>Avaliable Res W: </color>" + item.width + " H: "+item.height + " cam " + i);
                }
                
                if (devices[i].isFrontFacing)
                {
                webCameraTexture = new WebCamTexture(devices[i].name,720,480,25);
                    displayImage.uvRect = new Rect(0, 0, 1, -1);
                }
            }
            //webCameraTexture = new WebCamTexture();

            ////Vector2 size = GetComponent<RectTransform>().sizeDelta = new Vector2(1920, 1080);
            //displayImage.GetComponent<RectTransform>().sizeDelta = new Vector2(webCameraTexture.width, webCameraTexture.height);
            displayImage.material.mainTexture = webCameraTexture;
            webCameraTexture.Play();
            currentWebCam = 1;
            //StartCoroutine("WebCamSetup");
            print("End?");

            flashImage = new GameObject("Flash").AddComponent<RectTransform>().gameObject.AddComponent<RawImage>();
            //flashImage.GetComponent<RawImage>().uvRect = new Rect(1,1,1,0);
            flashImage.material = new Material(Shader.Find("UI/Default"));
            flashImage.material.color = Color.clear;
            flashImage.transform.parent = transform.parent;
            RectTransform flashRect = flashImage.GetComponent<RectTransform>();
            flashRect.anchoredPosition = webCamRect.anchoredPosition;
            flashRect.sizeDelta = webCamRect.sizeDelta;
            flashRect.rotation = webCamRect.rotation;

        }

        if (webCameraTexture.videoRotationAngle != 0)
        {
        float roateback = 360-webCameraTexture.videoRotationAngle;
        webCamRect.localEulerAngles = new Vector3(0f, 0f, roateback);

        }
        print("Checking WebCam" + " videoRatio " + ((float)webCameraTexture.width / (float)webCameraTexture.height) + " RotationAngle " + webCameraTexture.videoRotationAngle);

    }
 


    public static void RunFlash() {
        print("Flashing");
        instace.flashyStart = Time.timeSinceLevelLoad;
        instace.flashImage.material.color = Color.white;

        instace.isFlashing = true;
    }

   


    IEnumerable WebCamSetup()
    {
        while (webCameraTexture.width < 100)
        {

        yield return new WaitForSeconds(3);
        }

        if (!webCameraTexture.videoVerticallyMirrored)
        {
            displayImage.uvRect = new Rect(1, 0, -1, -1);

        }
        print("<color=blue>WebCamTexture.devices.Length </color>" + WebCamTexture.devices.Length);
        print("Checking WebCam" + " Mirroed " + webCameraTexture.videoVerticallyMirrored + " RotationAngle " + webCameraTexture.videoRotationAngle + " Height: " + (float)webCameraTexture.height);
        webCamRect.localEulerAngles = new Vector3(0f, 0f, webCameraTexture.videoRotationAngle);

        //if (!devices[currentWebCam].isFrontFacing)
        //{
        //    Vector2 v2 = displayImage.rectTransform.sizeDelta;
        //    if (true)
        //    {

        //    }
        //    displayImage.rectTransform.sizeDelta
        //}

        // change as user rotates iPhone or Android:

        int cwNeeded = webCameraTexture.videoRotationAngle;
        // Unity helpfully returns the _clockwise_ twist needed
        // guess nobody at Unity noticed their product works in counterclockwise:
        int ccwNeeded = -cwNeeded;

        // IF the image needs to be mirrored, it seems that it
        // ALSO needs to be spun. Strange: but true.
        if (webCameraTexture.videoVerticallyMirrored) ccwNeeded += 180;

        // you'll be using a UI RawImage, so simply spin the RectTransform
        webCamRect.localEulerAngles = new Vector3(0f, 0f, ccwNeeded);

        float videoRatio = (float)webCameraTexture.width / (float)webCameraTexture.height;

        // you'll be using an AspectRatioFitter on the Image, so simply set it
        displayImage.GetComponent<AspectRatioFitter>().aspectRatio = videoRatio;

        // alert, the ONLY way to mirror a RAW image, is, the uvRect.
        // changing the scale is completely broken.
        if (webCameraTexture.videoVerticallyMirrored)
            displayImage.uvRect = new Rect(1, 0, -1, 1);
        //rawImage.uvRect = new Rect(1, 0, -1, 1);  // means flip on vertical axis
        else
            displayImage.uvRect = new Rect(0, 0, 1, 1);  // means no flip

        // devText.text =
        //  videoRotationAngle+"/"+ratio+"/"+wct.videoVerticallyMirrored;
    }

    IEnumerable WebBackCamSetup()
    {
        while (webCameraTexture.width < 100)
        {

            yield return new WaitForSeconds(3);
        }

        // change as user rotates iPhone or Android:

        int cwNeeded = webCameraTexture.videoRotationAngle;
        // Unity helpfully returns the _clockwise_ twist needed
        // guess nobody at Unity noticed their product works in counterclockwise:
        int ccwNeeded = -cwNeeded;

        // IF the image needs to be mirrored, it seems that it
        // ALSO needs to be spun. Strange: but true.
        if (webCameraTexture.videoVerticallyMirrored) ccwNeeded += 180;

        // you'll be using a UI RawImage, so simply spin the RectTransform
        webCamRect.localEulerAngles = new Vector3(0f, 0f, ccwNeeded);

        float videoRatio = (float)webCameraTexture.width / (float)webCameraTexture.height;

        // you'll be using an AspectRatioFitter on the Image, so simply set it
        displayImage.GetComponent<AspectRatioFitter>().aspectRatio = videoRatio;

        // alert, the ONLY way to mirror a RAW image, is, the uvRect.
        // changing the scale is completely broken.
        if (webCameraTexture.videoVerticallyMirrored)
            displayImage.uvRect = new Rect(1, 0, -1, 1);
        //rawImage.uvRect = new Rect(1, 0, -1, 1);  // means flip on vertical axis
        else
            displayImage.uvRect = new Rect(0, 0, 1, 1);  // means no flip


        print("Checking WebCam2" + " videoRatio " + ((float)webCameraTexture.width / (float)webCameraTexture.height) + " RotationAngle " + webCameraTexture.videoRotationAngle );
        // devText.text =
        //  videoRotationAngle+"/"+ratio+"/"+wct.videoVerticallyMirrored;
    }
}
